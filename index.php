<!DOCTYPE html>

<html>
	
	<head>
		
		<meta charset="UTF-8" />
		<title>Верстка проекта &laquo;Ingenium&raquo;</title>
		
		<link href="/template/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/template/css/layout.css" rel="stylesheet" type="text/css" media="screen" />

	</head>
	
	<body>
		
		<div class="container">
			
			<header class="page-header">
				<h1>Верстка проекта &laquo;Ingenium&raquo;</h1>
			</header>
			
			<table class="table table-hover">
				<thead>
					<th>#</th>
					<th>Страница</th>
					<th>Готовность, %</th>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td><a href="pages/news.php" target="_blank">Новости</a></td>
						<td><span class="badge badge-error">0%</span></td>
					</tr>
				</tbody>
			</table>
	
		</div>
		
	</body>
	
</html>